const products = [{category:"fruits",name:"apple",mrp:"10"},
					  {category:"fruits",name:"mango",mrp:"20"},
					  {category:"fruits",name:"banana",mrp:"15"},
					  {category:"fruits",name:"pineapple",mrp:"50"},
					  {category:"fruits",name:"orange",mrp:"70"},
					  {category:"fruits",name:"watermelon",mrp:"43"},
					  {category:"fruits",name:"melon",mrp:"65"},
					  {category:"fruits",name:"grapes",mrp:"105"},
					  {category:"fruits",name:"cherry",mrp:"156"},
					  {category:"fruits",name:"blueberries",mrp:"34"},
					  {category:"vegetables",name:"onion",mrp:"76"},
					  {category:"vegetables",name:"carrot",mrp:"45"},
					  {category:"vegetables",name:"cauliflower",mrp:"2"},
					  {category:"vegetables",name:"eggplant",mrp:"23"},
					  {category:"vegetables",name:"potato",mrp:"76"},
					  {category:"vegetables",name:"mushrooms",mrp:"23"},
					  {category:"vegetables",name:"broccoli",mrp:"45"},
					  {category:"vegetables",name:"corn",mrp:"54"},
					  {category:"vegetables",name:"cucumber",mrp:"23"},
					  {category:"vegetables",name:"greenpepper",mrp:"2"},
					  {category:"dairy",name:"milk",mrp:"62"},
					  {category:"dairy",name:"paneer",mrp:"26"},
					  {category:"dairy",name:"eggs",mrp:"42"},
					  {category:"dairy",name:"cheese",mrp:"25"},
					  {category:"dairy",name:"bread",mrp:"42"},
					  {category:"dairy",name:"butter",mrp:"42"},
					  {category:"dairy",name:"ice-cream",mrp:"13"},
					  {category:"dairy",name:"yogurt",mrp:"1"},
					  {category:"dairy",name:"dips",mrp:"0.5"},
					  {category:"dairy",name:"whippedcream",mrp:"3"},
					  {category:"bscuits",name:"hide and seek",mrp:"440"},
					  {category:"bscuits",name:"parle-g",mrp:"403"},
					  {category:"bscuits",name:"krackjack",mrp:"440"},
					  {category:"bscuits",name:"monaco",mrp:"403"},
					  {category:"bscuits",name:"20-20 cookies",mrp:"4"},
					  {category:"bscuits",name:"milk shakti",mrp:"6"},
					  {category:"bscuits",name:"bourbon",mrp:"48"},
					  {category:"bscuits",name:"magix",mrp:"49"},
					  {category:"bscuits",name:"coconut cookies",mrp:"46"},
					  {category:"bscuits",name:"top",mrp:"40"}];


//generates search table

function searchItem() {

	$(".item-table").hide();

	var searchItem = document.getElementById("item").value;

	var itemTable = document.createElement("table");
	itemTable.setAttribute("class","item-table");
	var tableDiv = document.getElementById("div-table");
	tableDiv.appendChild(itemTable);

	let itemTh1 = document.createElement("th");
	let itemTh2 = document.createElement("th");
	let itemTh3 = document.createElement("th");
	let itemTh4 = document.createElement("th");

	let txtTh1 = document.createTextNode("Name");
	let txtTh2 = document.createTextNode("Category");
	let txtTh3 = document.createTextNode("MRP");
	let txtTh4 = document.createTextNode("Quantity");

	itemTh1.appendChild(txtTh1);
	itemTh2.appendChild(txtTh2);
	itemTh3.appendChild(txtTh3);
	itemTh4.appendChild(txtTh4);

	itemTable.appendChild(itemTh1);
	itemTable.appendChild(itemTh2);
	itemTable.appendChild(itemTh3);
	itemTable.appendChild(itemTh4);

	products.forEach(function(item) {

		if(item.name.includes(searchItem) || item.category.includes(searchItem) || item.price.includes(searchItem))
		{
	  		let itemTr = document.createElement("tr");

	  		let itemTd1 = document.createElement("td");
	  		let txtTd1 = document.createTextNode(item.name.charAt(0).toUpperCase() + item.name.slice(1));

	  		let itemTd2 = document.createElement("td");
	  		let txtTd2 = document.createTextNode(item.category);

	  		let itemTd3 = document.createElement("td");
	  		let txtTd3 = document.createTextNode(item.mrp);

	  		let itemTd4 = document.createElement("td");
	  		let txtTd4 = document.createElement("INPUT");
	  		txtTd4.setAttribute("type", "number");
	  		txtTd4.setAttribute("value", "0");
	  		txtTd4.setAttribute("min", "0");
	  		txtTd4.setAttribute("id", item.name);

	  		itemTd1.appendChild(txtTd1);
	  		itemTd2.appendChild(txtTd2);
	  		itemTd3.appendChild(txtTd3);
	  		itemTd4.appendChild(txtTd4);

	  		itemTr.appendChild(itemTd1);
	  		itemTr.appendChild(itemTd2);
	  		itemTr.appendChild(itemTd3);
	  		itemTr.appendChild(itemTd4);
	  		
	  		itemTable.appendChild(itemTr); 
	  	}	
	});
	
}

//resets search table

function resetBuy() {
	$(".item-table").hide();
}

//generates cart

function addCart() {

	$(".cart-table").hide();

	var cartTable = document.createElement("table");
	cartTable.setAttribute("class","cart-table")
	var tableCart = document.getElementById("div-cart");
	tableCart.appendChild(cartTable);

	let itemTh1 = document.createElement("th");
	let itemTh2 = document.createElement("th");

	let txtTh1 = document.createTextNode("Name");
	let txtTh2 = document.createTextNode("Quantity");

	itemTh1.appendChild(txtTh1);
	itemTh2.appendChild(txtTh2);

	cartTable.appendChild(itemTh1);
	cartTable.appendChild(itemTh2);

	products.forEach(function(item) {
		if(document.getElementById(item.name).value > 0)
		{
			let itemTr = document.createElement("tr");

		  	let itemTd1 = document.createElement("td");
		  	let txtTd1 = document.createTextNode(item.name.charAt(0).toUpperCase() + item.name.slice(1));

		  	let itemTd2 = document.createElement("td");
		  	let txtTd2 = document.createTextNode(document.getElementById(item.name).value);

			itemTd1.appendChild(txtTd1);
		  	itemTd2.appendChild(txtTd2);	

		  	itemTr.appendChild(itemTd1);
		  	itemTr.appendChild(itemTd2);

		  	cartTable.appendChild(itemTr);
		}
	});

}

//generates bill

function bill() {

	totalPrice = 0;

	$(".bill-table").hide();

	billTable = document.createElement("table");
	billTable.setAttribute("class","bill-table")
	var tablebill = document.getElementById("div-bill");
	tablebill.appendChild(billTable);

	let itemTh1 = document.createElement("th");
	let itemTh2 = document.createElement("th");
	let itemTh3 = document.createElement("th");
	let itemTh4 = document.createElement("th");

	let txtTh1 = document.createTextNode("Name");
	let txtTh2 = document.createTextNode("MRP");
	let txtTh3 = document.createTextNode("Quantity");
	let txtTh4 = document.createTextNode("price");

	itemTh1.appendChild(txtTh1);
	itemTh2.appendChild(txtTh2);
	itemTh3.appendChild(txtTh3);
	itemTh4.appendChild(txtTh4);

	billTable.appendChild(itemTh1);
	billTable.appendChild(itemTh2);
	billTable.appendChild(itemTh3);
	billTable.appendChild(itemTh4);


	products.forEach(function(item) {

		if (document.getElementById(item.name).value > 0) {
		
			let itemTr = document.createElement("tr");

		  	let itemTd1 = document.createElement("td");
		  	let txtTd1 = document.createTextNode(item.name.charAt(0).toUpperCase() + item.name.slice(1));

		  	let itemTd2 = document.createElement("td");
		  	let txtTd2 = document.createTextNode(item.mrp);

		  	let itemTd3 = document.createElement("td");
		  	let txtTd3 = document.createTextNode(document.getElementById(item.name).value);

		  	let itemTd4 = document.createElement("td");
		  	let txtTd4 = document.createTextNode(item.mrp*document.getElementById(item.name).value);

		  	totalPrice += item.mrp*document.getElementById(item.name).value;

			itemTd1.appendChild(txtTd1);
		  	itemTd2.appendChild(txtTd2);
		  	itemTd3.appendChild(txtTd3);
		  	itemTd4.appendChild(txtTd4);	

		  	itemTr.appendChild(itemTd1);
		  	itemTr.appendChild(itemTd2);
		  	itemTr.appendChild(itemTd3);
		  	itemTr.appendChild(itemTd4);

		  	billTable.appendChild(itemTr);
		  }
		
	});

	document.getElementById("total").innerHTML = "Total : $" + totalPrice;
	
}