const all_products = [{category:"fruits",names:"apple",mrp:"10"},
					  {category:"fruits",names:"mango",mrp:"20"},
					  {category:"fruits",names:"banana",mrp:"15"},
					  {category:"fruits",names:"pineapple",mrp:"50"},
					  {category:"fruits",names:"orange",mrp:"70"},
					  {category:"fruits",names:"watermelon",mrp:"43"},
					  {category:"fruits",names:"melon",mrp:"65"},
					  {category:"fruits",names:"grapes",mrp:"105"},
					  {category:"fruits",names:"cherry",mrp:"156"},
					  {category:"fruits",names:"blueberries",mrp:"34"},
					  {category:"vegetables",names:"onion",mrp:"76"},
					  {category:"vegetables",names:"carrot",mrp:"45"},
					  {category:"vegetables",names:"cauliflower",mrp:"2"},
					  {category:"vegetables",names:"eggplant",mrp:"23"},
					  {category:"vegetables",names:"potato",mrp:"76"},
					  {category:"vegetables",names:"mushrooms",mrp:"23"},
					  {category:"vegetables",names:"broccoli",mrp:"45"},
					  {category:"vegetables",names:"corn",mrp:"54"},
					  {category:"vegetables",names:"cucumber",mrp:"23"},
					  {category:"vegetables",names:"greenpepper",mrp:"2"},
					  {category:"dairy",names:"milk",mrp:"62"},
					  {category:"dairy",names:"paneer",mrp:"26"},
					  {category:"dairy",names:"eggs",mrp:"42"},
					  {category:"dairy",names:"cheese",mrp:"25"},
					  {category:"dairy",names:"bread",mrp:"42"},
					  {category:"dairy",names:"butter",mrp:"42"},
					  {category:"dairy",names:"ice-cream",mrp:"13"},
					  {category:"dairy",names:"yogurt",mrp:"1"},
					  {category:"dairy",names:"dips",mrp:"0.5"},
					  {category:"dairy",names:"whippedcream",mrp:"3"},
					  {category:"bscuits",names:"hide and seek",mrp:"440"},
					  {category:"bscuits",names:"parle-g",mrp:"403"},
					  {category:"bscuits",names:"krackjack",mrp:"440"},
					  {category:"bscuits",names:"monaco",mrp:"403"},
					  {category:"bscuits",names:"20-20 cookies",mrp:"4"},
					  {category:"bscuits",names:"milk shakti",mrp:"6"},
					  {category:"bscuits",names:"bourbon",mrp:"48"},
					  {category:"bscuits",names:"magix",mrp:"49"},
					  {category:"bscuits",names:"coconut cookies",mrp:"46"},
					  {category:"bscuits",names:"top",mrp:"40"},	
						]




function item_table(){

	$("table").hide();

	var buy = document.getElementById("select");
	in_product = document.getElementById("in_item").value.toLowerCase();

	const product_add = document.createElement("table");
	product_add.setAttribute("id", "table");
	product_add.setAttribute("border",1);
	product_add.style.borderCollapse = "collapse"

	const product_th1 = document.createElement("th");
	product_th1.textContent = "Name";
	product_add.appendChild(product_th1);

	const product_th2 = document.createElement("th");
	product_th2.textContent = "Category";
	product_add.appendChild(product_th2);

	const product_th3 = document.createElement("th");
	product_th3.textContent = "Mrp";
	product_add.appendChild(product_th3);

	const product_th4 = document.createElement("th");
	product_th4.textContent = "Quantity";
	product_add.appendChild(product_th4);


	buy.appendChild(product_add);
	
	all_products.forEach(item => {

		if ((item.names.includes(in_product) || item.category.includes(in_product) || item.mrp.includes(in_product) ) & in_product != ""){

			const table_tr = document.createElement("tr");
			table_tr.setAttribute("class", "tr");

			const table_td1 = document.createElement("td");
			table_td1.textContent  = item.names;


			const table_td2 = document.createElement("td");
			table_td2.textContent  = item.category;

			const table_td3 = document.createElement("td");
			table_td3.textContent  = item.mrp;

			const table_td4 = document.createElement("INPUT");
    		table_td4.setAttribute("type", "number");
    		table_td4.setAttribute("value", "0");
    		table_td4.setAttribute("class","quant")
    		

			table_tr.appendChild(table_td1);
			table_tr.appendChild(table_td2);
			table_tr.appendChild(table_td3);
			table_tr.appendChild(table_td4);
			product_add.appendChild(table_tr);
			console.log(in_product);
		}
	    });
}



function table_reset(){
	$('table').remove();
}

function cart(){
	$("table").hide();
	
	var cart = document.getElementById("cart");

	const product_add = document.createElement("table");
	product_add.setAttribute("id", "tab");
	product_add.setAttribute("border",1);
	product_add.style.borderCollapse = "collapse"

	const product_th1 = document.createElement("th");
	product_th1.textContent = "Name";
	product_add.appendChild(product_th1);

	const product_th4 = document.createElement("th");
	product_th4.textContent = "Quantity";
	product_add.appendChild(product_th4);

	cart.appendChild(product_add);	

	all_products.forEach(item => {

		if (true){

			const table_tr = document.createElement("tr");
			table_tr.setAttribute("class", "tr");

			const table_td1 = document.createElement("td");
			table_td1.textContent  = item.names;


			const table_td2 = document.createElement("td");
			table_td2.textContent  = document.getElementsByClassName("quant").value;

			
			table_tr.appendChild(table_td1);
			table_tr.appendChild(table_td2);

			product_add.appendChild(table_tr);
		}
	    });


}


function bill(){
	$("#main").hide();
	 var h1 = document.createElement("H1");
    var total_bill = document.createTextNode("Total Bill");
    h1.appendChild(total_bill);
    document.body.appendChild(h1);



}